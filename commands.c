#include "commands.h"

//Turn on some LEDs
void LED_ON (char * LED) {
	//Turn on an LED
	if (LED[0] == '0') {//Turn on all LEDs
		GPIOB_ODR |= 0xFF00;
	} else {
		GPIOB_ODR |= 1 << (LED[0] - 41);
	}
}

//Turn off some LEDs
void LED_OFF (char * LED) {
	//Turn off an LED
	if (LED[0] == '0') {
		GPIOB_ODR &= ~(0xFF00);
	} else {
		GPIOB_ODR &= ~(1 << (LED[0]-41));
	}
}

//Describe commands
void HELP (char * data) {
	SendLine("This is the help menu\r\n");
	SendLine("\"LED ON X\" will turn on LED X\r\n");
	SendLine("\"LED OFF X\" will turn off LED X\r\n");
	SendLine("\"LED ON 0\" will turn on all LEDs\r\n");
	SendLine("\"LED OFF 0\" will turn off all LEDs\r\n");
	SendLine("\"QUERY LED X\" will report the status of LED X\r\n");
	SendLine("\"INFO\" prints date and time of compilation\r\n");
	SendLine("\"HELP\" prints this help message\r\n");
}

//Return state of LED
void QUERY_LED (char * LED) {
	if (GPIOB_IDR & (1 << (LED[0]-41))) {
		SendLine("The LED is on\r\n");
	} else {
		SendLine("The LED is off\r\n");
	}
}

//Display date and time of compilation
void INFO (char * data) {
	SendLine("Compilation time and date:\r\n");
	SendLine(__TIME__);
	SendLine("\r\n");
	SendLine(__DATE__);
	SendLine("\r\n");
}
