/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Lab 2
*/

#include <stdint.h>
#include "registers.h"
#include "UART.h" //Enable UART functions
#include "commands.h"

//Dirty delay loop
void delay(unsigned count) {
	int i;
	for (i = 0; i < count; i++) {
	}
}

/*
*/

int main()
{
	uint8_t commandByte;
	char command[COMMAND_LENGTH] = {0};
	char data[COMMAND_LENGTH] = {0};
	unsigned charCount = 0;
	unsigned dataNum = 0;
	int i;//Count variables
	int j;
	//Need to setup the the LED's on the board
	RCC_APB2ENR |= 0x08; // Enable Port B clock
	GPIOB_ODR  &= ~0x0000FF00; // Switch off LEDs
	GPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
	
	Serial_Open();//Start serial communications
	
	SendLine(" \n\rSeaBiscuit > ");//Command prompt
	
	while(1) {
		commandByte = GetByte();
		if((commandByte == 0x8) || (commandByte == 0x7F)) {//Check for backspace
			if (charCount > 0) {
				SendByte(0x8);//Mirror the backspace
				SendByte(0x20);//Send a whitespace to clear a character
				SendByte(0x8);//Move back one spot
				
				//Remove the last byte in the command buffer and decrement count
				command[charCount] = 0;
				charCount -= 1;
			}
		} else {
			if (commandByte == 0xD) {//Check for enter key
				SendByte(0xA);//Send carriage return and line feed to the typewriter
				SendByte(0xD);
				
				if(charCount != 0) {
					//Execute command
					for (i = 0; i < COMMAND_LIST_LENGTH; i += 1) {
						for (j = 0; (j < COMMAND_LENGTH) && ((COMMANDLIST[i][j] == command[j]) || (COMMANDLIST[i][j] == 0x0)); j += 1) {
							//Look for complete match, get any data
							if (COMMANDLIST[i][j] == 0x0) {
								data[dataNum] = command[j];
								dataNum += 1;
							}
						}
						if (j == COMMAND_LENGTH) {
							functionCalls[i](data);
							break;
						}
					}
					
					if (i == COMMAND_LIST_LENGTH) {
						SendLine("Invalid command\n\r");
					}
					
					//Reset character strings
					charCount = 0;
					dataNum = 0;
					for (int k = 0; k < COMMAND_LENGTH; k += 1) {
						command[k] = 0;
						data[k] = 0;
					}
				}
				SendLine("SeaBiscuit > ");//Command prompt
			} else {
				if ((charCount < COMMAND_LENGTH) && (commandByte >= 0x20)) {
					//Add the character to the command and increment counter, ignoring garbage characters
					//Also, capitalize any lower-case characters
					if ((commandByte >= 'a') && (commandByte <= 'z')) {
						commandByte -= 32;
					}
					command[charCount] = commandByte;
					charCount += 1;
					//Mirror the byte
					SendByte(commandByte);
				}
			}
		}
	}
}
