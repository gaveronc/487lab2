/*
* This file defines a selection of useful registers such as
* clock registers and GPIO registers. It is likely that this
* file will be expanded as time goes on. Currently, the
* registers are all treated as pointers and need to be
* dereferenced accordingly in the main program when used.
*/

#ifndef REGISTERS_H
#define REGISTERS_H
#include <stdint.h>

#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB1ENR           (* (uint32_t volatile *)(RCC_BASE + 0x1C))
#define RCC_APB2ENR           (* (uint32_t volatile *)(RCC_BASE + 0x18))
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)

//GPIOB holds the LEDs on pins 8 through 15
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR             (* (uint32_t volatile *)(GPIOB_BASE + 0x0C))
#define GPIOB_CRL             (* (uint32_t volatile *)(GPIOB_BASE + 0x00))
#define GPIOB_CRH             (* (uint32_t volatile *)(GPIOB_BASE + 0x04))
#define GPIOB_BSRR            (* (uint32_t volatile *)(GPIOB_BASE + 0x10))
#define GPIOB_BRR             (* (uint32_t volatile *)(GPIOB_BASE + 0x14))
#define GPIOB_IDR             (* (uint32_t volatile *)(GPIOB_BASE + 0x08))

//GPIOA holds the UART TX/RX pins on pins 2 and 3, respectively
#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)
#define GPIOA_ODR             (* (uint32_t volatile *)(GPIOA_BASE + 0x0C))
#define GPIOA_CRL             (* (uint32_t volatile *)(GPIOA_BASE + 0x00))
#define GPIOA_CRH             (* (uint32_t volatile *)(GPIOA_BASE + 0x04))
#define GPIOA_BSRR            (* (uint32_t volatile *)(GPIOA_BASE  + 0x10))
#define GPIOA_BRR             (* (uint32_t volatile *)(GPIOA_BASE  + 0x14))
#define GPIOA_IDR             (* (uint32_t volatile *)(GPIOA_BASE + 0x08))

//USART 2 stuff
#define USART2_BASE           (PERIPH_BASE + 0x4400)
#define USART2_SR             (* (uint32_t volatile *)(USART2_BASE + 0x00))
#define USART2_DR             (* (uint32_t volatile *)(USART2_BASE + 0x04))
#define USART2_BRR            (* (uint32_t volatile *)(USART2_BASE + 0x08))
#define USART2_CR1            (* (uint32_t volatile *)(USART2_BASE + 0x0C))
#define USART2_CR2            (* (uint32_t volatile *)(USART2_BASE + 0x10))
#define USART2_CR3            (* (uint32_t volatile *)(USART2_BASE + 0x14))
#define USART2_GTPR           (* (uint32_t volatile *)(USART2_BASE + 0x18))

#endif      
